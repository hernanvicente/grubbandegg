# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

services = ['Düngung', 'Aussaat', 'Bodenbearbeitung', 'Pflanzenschutz', 'Grasernte', 'Getreideernte', 'Maisernte', 'Kommunale Dienstleistungen']

if Client.count.zero?
  Client.populate 100..200 do |client|
    client.first_name = Faker::Name.first_name
    client.last_name = Faker::Name.last_name
    client.company_name = Faker::CompanySE.name
    client.address = Faker::AddressDE.street_address
    client.zip_code = Faker::AddressDE.zip_code
    client.email = Faker::Internet.email
    client.website = Faker::Internet.http_url
    client.phones = Faker::PhoneNumberDE.home_work_phone_number
    client.note = Populator.paragraphs(2..3)
  end
end

if Employee.count.zero?
  Employee.populate 30..40 do |employee|
    employee.first_name = Faker::NameDE.first_name
    employee.last_name = Faker::NameDE.last_name
    employee.email = Faker::Internet.email
    employee.age = [18..55].sample
  end
  #Employee.all.each { |employee| employee.update_attributes(image: File.open(IMAGES.sample,'rb')) }
end

if Machine.count.zero?
  Machine.populate 30..40 do |machine|
    machine.code = Faker::Product.model
    machine.model = Faker::Vehicle.model
    machine.year = [1990..2014].sample
  end
end

if Service.count.zero?
  services.each do |service|
    Service.create name: service, hourly_rate: 11.00
  end
end

if Order.count.zero?
  @clients = Client.all
  @machines = Machine.all
  @service = Service.all
  Order.populate 30..50 do |order|
    order.client_id = @clients.sample
    order.machine_id = @machines.sample
    order.service_id = @service.sample
  end
end
