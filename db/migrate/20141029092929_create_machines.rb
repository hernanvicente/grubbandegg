class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.string :code
      t.string :model
      t.integer :year

      t.timestamps
    end
  end
end
