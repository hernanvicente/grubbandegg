class AddHoursToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :hours, :float
  end
end
