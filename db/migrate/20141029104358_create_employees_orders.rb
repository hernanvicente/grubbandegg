class CreateEmployeesOrders < ActiveRecord::Migration
  def change
    create_table :employees_orders do |t|
      t.belongs_to :orders
      t.belongs_to :employees
    end
  end
end
