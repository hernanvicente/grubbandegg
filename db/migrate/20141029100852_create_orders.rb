class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :client, index: true
      t.references :service, index: true
      t.references :machine, index: true
      t.text :notes

      t.timestamps
    end
  end
end
