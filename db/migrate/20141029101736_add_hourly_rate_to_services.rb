class AddHourlyRateToServices < ActiveRecord::Migration
  def change
    add_column :services, :hourly_rate, :float
  end
end
