class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :company_name
      t.string :address
      t.integer :zip_code
      t.string :email
      t.string :website
      t.string :phones
      t.text :note

      t.timestamps
    end
  end
end
