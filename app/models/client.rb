# == Schema Information
#
# Table name: clients
#
#  id           :integer          not null, primary key
#  first_name   :string(255)
#  last_name    :string(255)
#  company_name :string(255)
#  address      :string(255)
#  zip_code     :integer
#  email        :string(255)
#  website      :string(255)
#  phones       :string(255)
#  note         :text
#  created_at   :datetime
#  updated_at   :datetime
#

class Client < ActiveRecord::Base
  has_many :orders

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :company_name
  validates_presence_of :address

  def name
    first_name + " " + last_name
  end

end
