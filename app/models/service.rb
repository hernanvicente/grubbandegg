# == Schema Information
#
# Table name: services
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  hourly_rate :float
#

class Service < ActiveRecord::Base
  validates_presence_of :name
end
