# == Schema Information
#
# Table name: employees
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  last_name  :string(255)
#  email      :string(255)
#  age        :integer
#  created_at :datetime
#  updated_at :datetime
#

class Employee < ActiveRecord::Base
  has_and_belongs_to_many :orders, inverse_of: :employee
  validates_presence_of :first_name
  validates_presence_of :last_name

  def name
    first_name + " " + last_name
  end

  def worked_hours
    orders.sum(:hours)
  end

  def last_worked_order
    orders.last.name if orders.last.present?
  end

  rails_admin do
    list do
      field :name
      field :worked_hours
      field :last_worked_order
    end
  end


end
