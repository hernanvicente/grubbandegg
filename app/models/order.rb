# == Schema Information
#
# Table name: orders
#
#  id         :integer          not null, primary key
#  client_id  :integer
#  service_id :integer
#  machine_id :integer
#  notes      :text
#  created_at :datetime
#  updated_at :datetime
#  hours      :float
#
# Indexes
#
#  index_orders_on_client_id   (client_id)
#  index_orders_on_machine_id  (machine_id)
#  index_orders_on_service_id  (service_id)
#

class Order < ActiveRecord::Base
  belongs_to :client
  belongs_to :service
  belongs_to :machine
  has_and_belongs_to_many :employees, inverse_of: :order

  validates_associated :client, :service, :machine, :employees
  validates :client, :service, :machine, :employees, :hours, presence:true

  def name
    client.name + " - " + service.name + " - " + hours.to_s
  end

  rails_admin do
    list do
      field :client
      field :service
      field :employees
      field :machine
      field :hours
      field :notes
      field :created_at
    end
  end

end
