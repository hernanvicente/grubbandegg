# == Schema Information
#
# Table name: machines
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  model      :string(255)
#  year       :integer
#  created_at :datetime
#  updated_at :datetime
#

class Machine < ActiveRecord::Base
  validates_presence_of :code
  validates_presence_of :model

  def name
    code + " - " + model
  end

end
